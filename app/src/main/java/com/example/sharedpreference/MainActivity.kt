package com.example.sharedpreference

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE)

        val email = sharedPreferences.getString("email", "")
        val firstname = sharedPreferences.getString("firstname", "")
        val lastname = sharedPreferences.getString("lastname", "")
        val age = sharedPreferences.getString("age", "")
        val address = sharedPreferences.getString("address", "")

        email_et.setText(email)
        firstName_et.setText(firstname)
        lastName_et.setText(lastname)
        age_et.setText(age)
        address_et.setText(address)

        save_button.setOnClickListener {
            if (email_et.text.toString().isNotEmpty() && firstName_et.text.toString().
                    isNotEmpty()
                && lastName_et.text.toString().isNotEmpty() && age_et.text.toString()
                    .isNotEmpty()
                && address_et.text.toString().isNotEmpty()
            ){
                save()
                Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show()

            } else
            {   Toast.makeText(applicationContext, "Some Fields Are Empty", Toast.LENGTH_SHORT).show() }
        }
    }


    fun save() {
        val email = email_et.text.toString()
        val firstname = firstName_et.text.toString()
        val lastname = lastName_et.text.toString()
        val age = age_et.text.toString()
        val address = address_et.text.toString()

        val editor = sharedPreferences.edit()
        editor.putString("email", email)
        editor.putString("firstname", firstname)
        editor.putString("lastname", lastname)
        editor.putString("age", age)
        editor.putString("address", address)
        editor.apply()
    }

}
